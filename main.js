// Add a custom menu to the active spreadsheet.
function onOpen(e) {
  SpreadsheetApp.getUi()
      .createMenu('Custom')
      .addItem('alert', 'alert')
      .addSeparator()
      .addItem('test', 'test')
      .addToUi();
}


function alert(){
  var ui = SpreadsheetApp.getUi();
  ui.alert(SpreadsheetApp.getActiveRange().getValue());
}

function test(){
  //code here
}
